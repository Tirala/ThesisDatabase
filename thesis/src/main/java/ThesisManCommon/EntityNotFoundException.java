package ThesisManCommon;

/**
 *
 * @author Kristina Miklasova, Peter Tirala
 */
public class EntityNotFoundException extends RuntimeException {
    
    /**
     * Constructs an instance of <code>EntityNotFoundException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public EntityNotFoundException(String msg) {
        super(msg);
    }
    
}
