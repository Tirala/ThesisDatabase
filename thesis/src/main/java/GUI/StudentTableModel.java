package GUI;

import ThesisMan.Student;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kristina Miklasova, Peter Tirala
 */
public class StudentTableModel extends AbstractTableModel {
     
    final static Logger log = LoggerFactory.getLogger(StudentTableModel.class);
    private final ArrayList<Student> students = new ArrayList<>();
   
    public Student getStudentAtRow(int row){
        return students.get(row);
    }
    
    @Override
    public int getRowCount() {
        return students.size();
    }
   
    @Override
    public int getColumnCount() {
        return 3;
    }
    
    public void addStudent(Student student) {
        students.add(student);
        int lastRow = students.size() - 1;
        log.debug("Added student " + student + " into table at " + lastRow + " row");
        fireTableRowsInserted(lastRow, lastRow);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Student student = students.get(rowIndex);
        switch (columnIndex) {
            case 0: 
                return student.getId();
            case 1: 
                return student.getName();
            case 2:
                return student.getSurname();
            default:
                log.error("Nonexisting column index " + columnIndex);
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return java.util.ResourceBundle.getBundle("GUI/Bundle").getString("ID");
            case 1:
                return java.util.ResourceBundle.getBundle("GUI/Bundle").getString("NAME");
            case 2:
                return java.util.ResourceBundle.getBundle("GUI/Bundle").getString("SURNAME");
           
            default:
                log.error("Nonexisting column index " + columnIndex);
                throw new IllegalArgumentException("columnIndex");
        }
    }
 
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return Long.class;
            case 1:
            case 2:         
                return String.class;
            default:
                log.error("Nonexisting column index " + columnIndex);
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Student student = students.get(rowIndex);
        switch (columnIndex) {
            case 0:
                student.setId((Long) aValue);
                break;
            case 1:
                student.setName((String) aValue);
                break;
            case 2:
                 student.setSurname((String) aValue);
                break;
            default:
                 log.error("Nonexisting column index " + columnIndex);
                throw new IllegalArgumentException("columnIndex");
        }
        log.debug("Student table updated at row " + rowIndex + " and column " + columnIndex);
        fireTableCellUpdated(rowIndex, columnIndex);   
    }
 
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return false;
            case 1: 
            case 2:
                return true;      
            default:
                log.error("Non-editable column index " + columnIndex);
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    public void removeRow(int row){
        students.remove(row);
        log.debug("Deleted " + row + " row in student table");
        fireTableRowsDeleted(row, row);
    }
}
