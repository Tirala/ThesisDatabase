package GUI;

import ThesisMan.Student;
import ThesisMan.Thesis;
import ThesisMan.Typ;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kristina Miklasova, Peter Tirala
 */
public class ThesisTableModel extends AbstractTableModel {
    final static org.slf4j.Logger log = LoggerFactory.getLogger(ThesisTableModel.class);
    private final ArrayList<Thesis> theses = new ArrayList<>();
    
    public Thesis getThesisAtRow(int row) {
        return theses.get(row);
    }
         
    @Override
    public int getRowCount() {
        return theses.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Thesis thesis = theses.get(rowIndex);
        switch (columnIndex) {
            case 0:
               return thesis.getId();
            case 1:
               return thesis.getName();
            case 2:
                return thesis.getYear();
            case 3:
                return thesis.getType();
            case 4:
                return thesis.getAuthor();
            default:
                log.error("Nonexisting column index " + columnIndex);
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    public void addThesis(Thesis thesis) {
        theses.add(thesis);
        int lastRow = theses.size() - 1;
        log.debug("Added thesis " + thesis + " into table at " + lastRow + " row");
        fireTableRowsInserted(lastRow, lastRow);
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return java.util.ResourceBundle.getBundle("GUI/Bundle").getString("ID");
            case 1:
                return java.util.ResourceBundle.getBundle("GUI/Bundle").getString("NAME_T");
            case 2:
                return java.util.ResourceBundle.getBundle("GUI/Bundle").getString("YEAR");
            case 3:
                return java.util.ResourceBundle.getBundle("GUI/Bundle").getString("TYPE");
            case 4:
                return java.util.ResourceBundle.getBundle("GUI/Bundle").getString("AUTHOR");
            default:
                log.error("Nonexisting column index " + columnIndex);
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    @Override
     public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:           
                return Long.class;
            case 1: 
               return String.class;
            case 2:
                return Integer.class;
            case 3:
                return Typ.class;
            case 4:
                return Student.class;
            default:
                log.error("Nonexisting column index " + columnIndex);
                throw new IllegalArgumentException("columnIndex");
        }
    }
     
     @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Thesis thesis = theses.get(rowIndex);
        switch (columnIndex) {
            case 0:
                thesis.setId((Long) aValue);
                break;
            case 1:
                thesis.setName((String) aValue);
                break; 
            case 2:
                thesis.setYear((int) aValue);
                break;
            case 3:
                thesis.setType((Typ) aValue);
                break;
            case 4:                              
                thesis.setAuthor((Student)aValue);
                break;
            default:
                log.error("Nonexisting column index " + columnIndex);
                throw new IllegalArgumentException("columnIndex");
        }
        log.debug("Thesis table updated at " + rowIndex + " row and " + columnIndex + " column");
        fireTableCellUpdated(rowIndex, columnIndex);    
    }
     
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0: 
                return false;
            case 1:
            case 2:
            case 3:
            case 4:
                return true;      
            default:
                log.error("Non-editable column index " + columnIndex);
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    public void removeRow(int row){
        theses.remove(row);
        log.debug("Deleted " + row + " row in thesis table");
        fireTableRowsDeleted(row, row);
    }   
}
