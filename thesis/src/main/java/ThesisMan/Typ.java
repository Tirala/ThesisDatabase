package ThesisMan;

import java.util.ResourceBundle;

/**
 * Enumeration for the type of thesis.
 * 
 * @author Kristina Miklasova, Peter Tirala
 */
public enum Typ {
    BACHELOR, MASTER, PHD;
}