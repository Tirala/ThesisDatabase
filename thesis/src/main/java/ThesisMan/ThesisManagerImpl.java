package ThesisMan;

import ThesisManCommon.DBUtils;
import ThesisManCommon.ValidationException;
import ThesisManCommon.ServiceFailureException;
import ThesisManCommon.IllegalEntityException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.sql.DataSource;
import org.slf4j.LoggerFactory;


/**
 * Implements methods for ThesisManager.
 * @author Kristina Miklasova, Peter Tirala
 */
public class ThesisManagerImpl implements ThesisManager {
    
    final static org.slf4j.Logger log = LoggerFactory.getLogger(ThesisManagerImpl.class);
    private final DataSource dataSource;
   
     
    public  ThesisManagerImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    
    private void checkDataSource() {
        if (dataSource == null) {
            log.error("Data source is null");
            throw new IllegalStateException("DataSource is not set");
        }
    }

    @Override
    public void createThesis(Thesis thesis) throws ServiceFailureException {
        
        checkDataSource();
        validate(thesis);
        if (thesis.getId() != null) {
            log.error("ID of thesis " + thesis +  "is already set");
            throw new IllegalEntityException("thesis id is already set");
        }
        Connection connection = null;
        PreparedStatement st =null;
        try {
             connection = dataSource.getConnection();     
             connection.setAutoCommit(false);
             st = connection.prepareStatement(
                 "INSERT INTO THESIS (thName, type, yearOfPublication, authorId) VALUES (?, ?, ?, ?)",
                Statement.RETURN_GENERATED_KEYS);
            
            st.setString(1, thesis.getName());
            st.setString(2, thesis.getType().toString());
            st.setInt(3, thesis.getYear());
            st.setLong(4, thesis.getAuthor().getId());
                       
            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count,thesis, true);

            Long id = DBUtils.getId(st.getGeneratedKeys());
            thesis.setId(id);
            log.debug("Created thesis " + thesis + " in db");
            connection.commit();
        } catch (SQLException ex) {
            log.error("Can not insert thesis " + thesis + " into db");
            String msg = "Error when inserting thesis into db";
            throw new ServiceFailureException(msg, ex);
        } finally {
            DBUtils.doRollbackQuietly(connection);
            DBUtils.closeQuietly(connection, st);
        }
    }
    
   

    @Override
    public void updateThesis(Thesis thesis) throws ServiceFailureException {
    
        checkDataSource();
        validate(thesis);
        if (thesis.getId() == null) {
            log.error("Thesis ID is null");
            throw new IllegalEntityException("thesis id is null");
        }
        
        Connection connection =null;
        PreparedStatement st =null;
        try{
             connection = dataSource.getConnection();
             connection.setAutoCommit(false);
             st = connection.prepareStatement(
                "UPDATE Thesis SET thName = ?, yearOfPublication = ?, type = ?,  authorId = ? WHERE thId = ?");
            st.setString(1, thesis.getName());
            st.setInt(2, thesis.getYear());
            st.setObject(3, thesis.getType().toString());
            st.setLong(4, thesis.getAuthor().getId());
            st.setLong(5, thesis.getId());
            
            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count,thesis, true);
            connection.commit();
            log.debug("Updated thesis " + thesis + " in db");
        } catch (SQLException ex) {
            log.error("Can not update thesis " + thesis + " into db");
            String msg = "Error when updating thesis into db";
            throw new ServiceFailureException(msg, ex);
        } finally {
            DBUtils.doRollbackQuietly(connection);
            DBUtils.closeQuietly(connection, st);
        }
    }

    @Override
    public void deleteThesis(Thesis thesis) throws ServiceFailureException {
        checkDataSource();
        if (thesis == null) {
            log.error("Thesis is null");
            throw new IllegalArgumentException("thesis is null");
        }
        if (thesis.getId() == null) {
            log.error("Thesis ID is null");
            throw new IllegalEntityException("thesis id is null");
        }
        Connection connection = null;
        PreparedStatement st = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
                st = connection.prepareStatement(
               "DELETE FROM thesis WHERE thId = ?");
            
            st.setLong(1, thesis.getId());
           
            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count,thesis, true);
            connection.commit();
            log.debug("Deleted thesis " + thesis + " from db");
        } catch (SQLException ex) {
            log.error("Can not delete thesis " + thesis + " in db");
            String msg = "Error when deleting thesis into db";
            throw new ServiceFailureException(msg, ex);
        } finally {
            DBUtils.doRollbackQuietly(connection);
            DBUtils.closeQuietly(connection, st);
        }
    }

    @Override
    public Thesis getThesisById(Long id) throws ServiceFailureException {
        checkDataSource();
        if (id == null) {
            log.error("ID of thesis is null");
            throw new IllegalArgumentException("id is null");
        }
        
        try (Connection connection = dataSource.getConnection();
             PreparedStatement st = connection.prepareStatement(
             "SELECT thId, thName, type, yearOfPublication, stId, stName, surname FROM Thesis JOIN Student" 
                     + " ON stId = authorId WHERE thId = ?")) {
                            
                st.setLong(1, id);
                ResultSet rs = st.executeQuery();
               
                if (rs.next()) {                   
                    Thesis thesis = rsToThesisWithStudent(rs);
                   
                    if (rs.next()) {
                        log.error("More entities with the same id found "
                            + "(source id: " + id + ", found " + thesis + " and " + resultSetToThesis(rs));
                        throw new ServiceFailureException(
                            "Internal error: More entities with the same id found "
                            + "(source id: " + id + ", found " + thesis + " and " + resultSetToThesis(rs));
                    }
                    
                    log.debug("Found thesis " + thesis + " with ID " + id + " in db");
                    return thesis;
                    
                } else {
                    return null;
                }
                
        } catch (SQLException ex) {
            log.error("Error with retrieving thesis with id " + id);
            throw new ServiceFailureException(
                    "Error when retrieving thesis with id " + id, ex);
        } 
    }
    
    private Thesis resultSetToThesis(ResultSet rs) throws SQLException {
        Thesis thesis = new Thesis();
        thesis.setId(rs.getLong("thId"));
        thesis.setName(rs.getString("thName"));
        thesis.setYear(rs.getInt("yearOfPublication"));
        thesis.setType(Typ.valueOf(rs.getString("type")));

        return thesis;
    }

    private Thesis rsToThesisWithStudent(ResultSet rs) throws SQLException {
        Thesis thesis = new Thesis();
         
        Student student = new Student();
        student.setName(rs.getString("stName"));
        student.setSurname(rs.getString("surname"));
        student.setId(rs.getLong("stId"));    
        
        thesis.setId(rs.getLong("thId"));
        thesis.setName(rs.getString("thName"));
        thesis.setYear(rs.getInt("yearOfPublication"));
        thesis.setType(Typ.valueOf(rs.getString("type")));
 
        thesis.setAuthor(student);
        
        return thesis;
    }
    
    @Override
     public List<Thesis> getAllTheses() throws ServiceFailureException {
        checkDataSource();
       
        try (Connection connection = dataSource.getConnection();
             PreparedStatement st = connection.prepareStatement(
             "SELECT thId, thName, yearOfPublication, type, stName, surname, stId "
                     + "FROM Thesis JOIN Student ON stId = authorId ")) {
                          
              ResultSet rs = st.executeQuery();
              List<Thesis> result = new ArrayList<>();
              
              while (rs.next()) {
                  Thesis thesis = rsToThesisWithStudent(rs);
                  result.add(thesis);
              }
                      
              log.debug("Getting all " + result.size() + " theses from db");
              return result;
        } catch (SQLException ex) {
            log.error("Error when retrieving all theses");
            throw new ServiceFailureException(
                    "Error when retrieving all theses", ex);
        }       
     }
     
    @Override
     public List<Thesis> getThesesForStudent(Student student) throws ServiceFailureException {
        checkDataSource();
        if (student == null) {
            log.error("Student is null - can not get all theses for student");
            throw new IllegalArgumentException("student is null");
        }
        try (Connection connection = dataSource.getConnection();
             PreparedStatement st = connection.prepareStatement(
             "SELECT thId, thName, yearOfPublication, type, authorId FROM thesis WHERE authorId = ?")) {
            
                st.setLong(1, student.getId());
                ResultSet rs = st.executeQuery();
                
                List<Thesis> result = new ArrayList<>();
                while (rs.next()) {                   
                    Thesis thesis = resultSetToThesis(rs);
                    thesis.setAuthor(student);
                   
                    result.add(thesis);  
                } 
                log.debug("Student " + student + " has " + result.size() + " theses in db");
                return result;
                               
        } catch (SQLException ex) {
            log.error("Error when retrieving all theses for student " + student);
            throw new ServiceFailureException(
                    "Error when retrieving all theses for student " + student, ex);
        }
    }
    
     private Long getKey(ResultSet keyRS, Thesis thesis) throws ServiceFailureException, SQLException {
        if (keyRS.next()) {
            if (keyRS.getMetaData().getColumnCount() != 1) {
                log.error("Generated key retriving failed when trying to insert thesis " + thesis
                        + " - wrong key fields count: " + keyRS.getMetaData().getColumnCount());
                throw new ServiceFailureException("Internal Error: Generated key"
                        + "retriving failed when trying to insert thesis " + thesis
                        + " - wrong key fields count: " + keyRS.getMetaData().getColumnCount());
            }
            Long result = keyRS.getLong(1);
            if (keyRS.next()) {
                log.error("Generated key retriving failed when trying to insert thesis " + thesis
                        + " - more keys found");
                throw new ServiceFailureException("Internal Error: Generated key"
                        + "retriving failed when trying to insert thesis " + thesis
                        + " - more keys found");
            }
            
            log.debug("Getting key for thesis " + thesis);
            return result;
        } else {
            log.error("Generated key retriving failed when trying to insert thesis " + thesis
                    + " - no key found");
            throw new ServiceFailureException("Internal Error: Generated key"
                    + "retriving failed when trying to insert thesis " + thesis
                    + " - no key found");
        }        
    }

      public void validate(Thesis thesis) throws IllegalArgumentException {
        if (thesis == null) {
            log.error("Thesis is null");
            throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("GUI/Bundle").getString("THESIS IS NULL"));
        }
        if (thesis.getName() == null) {
            log.error("Thesis name is null");
            throw new ValidationException(java.util.ResourceBundle.getBundle("GUI/Bundle").getString("THESIS NAME IS NULL"));
        }
        if (thesis.getName().equals("")) {
            log.error("Thesis name is empty");
            throw new ValidationException(java.util.ResourceBundle.getBundle("GUI/Bundle").getString("THESIS NAME IS EMPTY"));
        }           
        if (thesis.getAuthor() == null) {
            log.error("Thesis author is null");
            throw new ValidationException(java.util.ResourceBundle.getBundle("GUI/Bundle").getString("AUTHOR IS NULL"));
        }
        if (thesis.getYear() < 0 ) {
            log.error("Thesis year is negative");
            throw new ValidationException(java.util.ResourceBundle.getBundle("GUI/Bundle").getString("YEAR IS NEGATIVE"));
        }
        if (thesis.getType() == null) {
            log.error("Thesis type is null");
            throw new ValidationException(java.util.ResourceBundle.getBundle("GUI/Bundle").getString("TYPE IS NULL"));
        }
    }
     
}
