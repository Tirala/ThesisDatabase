package ThesisMan;

import ThesisManCommon.DBUtils;
import ThesisManCommon.ValidationException;
import ThesisManCommon.ServiceFailureException;
import ThesisManCommon.IllegalEntityException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import javax.sql.DataSource;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * This class implements StudentManager.
 * 
 * @author Kristina Miklasova, Peter Tirala
 */
public class StudentManagerImpl implements StudentManager {

    final static Logger log = LoggerFactory.getLogger(StudentManagerImpl.class);
    private final DataSource dataSource; 

    public  StudentManagerImpl(DataSource dataSource){
        this.dataSource = dataSource;
    }
    
    private void checkDataSource() {
        if (dataSource == null) {
            log.error("Data source si not set");
            throw new IllegalStateException("DataSource is not set");
        }
    }
    
   @Override
    public void createStudent(Student student) throws ServiceFailureException {
        checkDataSource();
        validate(student);
        if (student.getId() != null) {
            log.error("Student with the same ID already exist");
            throw new IllegalEntityException("student id is already set");
        }
        Connection connection = null;
        PreparedStatement st = null;
        try{
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            
            st = connection.prepareStatement(
                 "INSERT INTO STUDENT (stName, surname) VALUES (?, ?)",
                Statement.RETURN_GENERATED_KEYS);
            
            st.setString(1, student.getName());
            st.setString(2, student.getSurname());
            
            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count, student, true);
            
            Long id = DBUtils.getId(st.getGeneratedKeys());
            student.setId(id);
            connection.commit();
            log.debug("Created student " + student + " in db");
        } catch (SQLException ex) {          
            log.error("Can not insert student " + student + " into db");
            String msg = "Error when inserting student into db";
            throw new ServiceFailureException(msg, ex);
        } finally {
            DBUtils.doRollbackQuietly(connection);
            DBUtils.closeQuietly(connection, st);
        }
    } 
    
   
    
    @Override
    public void updateStudent(Student student) throws ServiceFailureException {
        checkDataSource();
        validate(student);
        if (student.getId() == null) {
            log.error("ID of student " + student + " is null");
            throw new IllegalEntityException("student id is null");
        }
        
        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            st = conn.prepareStatement(
                    "UPDATE Student SET stName = ?, surname = ? WHERE stId = ?");
            st.setString(1, student.getName());
            st.setString(2, student.getSurname());
            st.setLong(3, student.getId());

            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count, student, false);
            conn.commit();
            log.debug("Updated student " + student + " in db");
        } catch (SQLException ex) {
            log.error("Can not update student " + student + " in db");
            String msg = "Error when updating student in db";
            log.error(msg);
            throw new ServiceFailureException(msg, ex);
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        }
    }

    @Override
    public void deleteStudent(Student student) throws ServiceFailureException {
        checkDataSource();
        if (student == null) {
            log.error("Student " + student+ " is null");
            throw new IllegalArgumentException("student is null");
        }
        if (student.getId() == null) {
            log.error("ID of student "  + student + " is null");
            throw new IllegalEntityException("student id is null");
        }
        
        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            st = conn.prepareStatement(
                    "DELETE FROM Student WHERE stId = ?");
            st.setLong(1, student.getId());

            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count, student, false);
            conn.commit();
            log.debug("Deleted student " + student + " from db");
        } catch (SQLException ex) {
            log.error("Can not delete student " + student + " from the db");
            String msg = "Error when deleting student from the db";
            throw new ServiceFailureException(msg, ex);
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        }
    }

    @Override
    public Student getStudentById(Long id) throws ServiceFailureException {
        checkDataSource();
        if (id == null) {
            log.error("ID is null");
            throw new IllegalArgumentException("id is null");
        }
        
        try ( Connection connection = dataSource.getConnection();
            PreparedStatement st = connection.prepareStatement(
            "SELECT stId, stName, surname FROM student WHERE stId = ?")) {
                st.setLong(1, id);
                ResultSet rs = st.executeQuery();
            
                if (rs.next()) {
                    Student student = resultSetToStudent(rs);
                
                    if (rs.next()) {
                        log.error("More entities with the same id found "
                            + "(source id: " + id + ", found " + student + " and " + resultSetToStudent(rs));
                        throw new ServiceFailureException(
                            "Internal error: More entities with the same id found "
                            + "(source id: " + id + ", found " + student + " and " + resultSetToStudent(rs));
                    }
                
                    log.debug("Found student " + student + " with ID " + id + " in db");
                    return student;
                
                } else {
                    return null;
                }
                
        } catch (SQLException ex) {
            log.error("Error when retrieving student with id " + id);
            throw new ServiceFailureException(
            "Error when retrieving student with id " + id, ex);
        }
        
    }

    private Student resultSetToStudent(ResultSet rs) throws SQLException {
        Student student = new Student();
        student.setId(rs.getLong("stId"));
        student.setName(rs.getString("stName"));
        student.setSurname(rs.getString("surname"));
        return student;
    }
    
    @Override
    public List<Student> findAllStudents() throws ServiceFailureException {
        checkDataSource();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement st = connection.prepareStatement(
             "SELECT stId, stName, surname FROM student")) {
            
            ResultSet rs = st.executeQuery();
            
            List<Student> result = new ArrayList<>();
            while (rs.next()) {
                result.add(resultSetToStudent(rs));
            }
            log.debug("Getting all " + result.size() + " students from db");
            return result;
            
        } catch (SQLException ex) {
            log.error("Can not select all students");
            throw new ServiceFailureException(
                    "Error when retrieving all students", ex);
        }
    }   
    
     public void validate(Student student) throws IllegalArgumentException {
        if (student == null) {
            log.error("Studen is null");
            throw new IllegalArgumentException(java.util.ResourceBundle.getBundle("GUI/Bundle").getString("STUDENT IS NULL"));
        }
        if (student.getName() == null) {
            log.error("student name is null");
            throw new ValidationException(java.util.ResourceBundle.getBundle("GUI/Bundle").getString("STUDENT NAME IS NULL"));
        }
        if (student.getSurname() == null ) {
            log.error("student surname is null");
            throw new ValidationException(java.util.ResourceBundle.getBundle("GUI/Bundle").getString("STUDENT SURNAME IS NULL"));
        }
        if (student.getName().equals("")) {
            log.error("student name is empty");
            throw new ValidationException(java.util.ResourceBundle.getBundle("GUI/Bundle").getString("STUDENT NAME IS EMPTY"));
        }
        if (student.getSurname().equals("")) {
            log.error("student surname is empty");
            throw new ValidationException(java.util.ResourceBundle.getBundle("GUI/Bundle").getString("STUDENT SURNAME IS EMPTY"));
        }
        if (!Pattern.matches("[a-zA-Z ]+", student.getName())) {
            log.error("student name does not contain only characters");
            throw new ValidationException(java.util.ResourceBundle.getBundle("GUI/Bundle").getString("STUDENT NAME DOES NOT CONTAIN ONLY CHARACTERS"));
        }
        if (!Pattern.matches("[a-zA-Z ]+", student.getSurname())) {
            log.error("student surname does not contain only characters");
            throw new ValidationException(java.util.ResourceBundle.getBundle("GUI/Bundle").getString("STUDENT SURNAME DOES NOT CONTAIN ONLY CHARACTERS"));
        }      
    }
    
    private Long getKey(ResultSet keyRS, Student student) throws ServiceFailureException, SQLException {
        if (keyRS.next()) {
            if (keyRS.getMetaData().getColumnCount() != 1) {
                log.error("Generated key retriving failed when trying to insert student " + student
                        + " - wrong key fields count: " + keyRS.getMetaData().getColumnCount());
                throw new ServiceFailureException("Internal Error: Generated key"
                        + "retriving failed when trying to insert student " + student
                        + " - wrong key fields count: " + keyRS.getMetaData().getColumnCount());
            }
            Long result = keyRS.getLong(1);
            if (keyRS.next()) {
                log.error("Generated key retriving failed when trying to insert student " + student
                        + " - more keys found");
                throw new ServiceFailureException("Internal Error: Generated key"
                        + "retriving failed when trying to insert student " + student
                        + " - more keys found");
            }
            log.debug("Getting key for student " + student);
            return result;
        } else {
            log.error("Generated key retriving failed when trying to insert student " + student
                    + " - no key found");
            throw new ServiceFailureException("Internal Error: Generated key"
                    + "retriving failed when trying to insert student " + student
                    + " - no key found");
        }        
    }
    
}
